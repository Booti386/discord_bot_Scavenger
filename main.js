// Join URL: https://discordapp.com/api/oauth2/authorize?response_type=code&client_id=xxxxx&scope=bot&redirect_uri=https://myuri.com/

if (!console)
	console = {};
if (!console.log)
	console.log = () => {};

function Bot()
{
	this.OP_DISPATCH = 0;
	this.OP_HEARTBEAT = 1;
	this.OP_IDENT = 2;
	this.OP_UPD_STATUS = 3;
	this.OP_REQ_GUILD_MEMBERS = 8;
	this.OP_INVAL_SESSION = 9;
	this.OP_HELLO = 10;
	this.OP_HEARTBEAT_ACK = 11;

	this.GW_INTENT_GUILD = 1 << 0;
	this.GW_INTENT_GUILD_MEMBERS = 1 << 1;
	this.GW_INTENT_GUILD_BANS = 1 << 2;
	this.GW_INTENT_GUILD_EMOJIS = 1 << 3;
	this.GW_INTENT_GUILD_INTEGRATIONS = 1 << 4;
	this.GW_INTENT_GUILD_WEBHOOKS = 1 << 5;
	this.GW_INTENT_GUILD_INVITES = 1 << 6;
	this.GW_INTENT_GUILD_VOICE_STATES = 1 << 7;
	this.GW_INTENT_GUILD_PRESENCES = 1 << 8;
	this.GW_INTENT_GUILD_MESSAGES = 1 << 9;
	this.GW_INTENT_GUILD_MESSAGE_REACTIONS = 1 << 10;
	this.GW_INTENT_GUILD_MESSAGE_TYPING = 1 << 11;
	this.GW_INTENT_DIRECT_MESSAGES = 1 << 12;
	this.GW_INTENT_DIRECT_MESSAGE_REACTIONS = 1 << 13;
	this.GW_INTENT_DIRECT_MESSAGE_TYPING = 1 << 14;

	this.GW_ERR_UNK_ERROR = 4000;
	this.GW_ERR_UNK_OPCODE = 4001;
	this.GW_ERR_DECODE_ERR = 4002;
	this.GW_ERR_NOT_AUTH = 4003;
	this.GW_ERR_AUTH_FAILED = 4004;
	this.GW_ERR_ALREADY_AUTH = 4005;
	this.GW_ERR_INVAL_SEQ = 4007;
	this.GW_ERR_RATE_LIMITED = 4008;
	this.GW_ERR_SESSION_TIMEOUT = 4009;
	this.GW_ERR_INVAL_SHARD = 4010;
	this.GW_ERR_SHARD_REQ = 4011;
	this.GW_ERR_INVAL_API_VER = 4012;
	this.GW_ERR_INVAL_INTENT = 4013;
	this.GW_ERR_DISALLOWED_INTENT = 4014;

	this.STATE_INIT_WS = 0;
	this.STATE_CONNECTED = 1;

	this.api_http_version = 6;
	this.api_gateway_version = 6;
	this.api_root = 'https://discordapp.com/api';
	this.res_root = 'https://a9eb9b1f190649999c5e66d7202f0108.cors-proxy.bitmycode.com/';

	this.state = this.STATE_INIT_WS;
	this.api_gateway = null;
	this.ws = null;
	this.last_seq = null;
	this.heartbeat_interval = 20000;
	this.heartbeat_interval_id = 0;

	this.guilds = {};

	let token_elem = document.getElementById('scavenger_token');
	this.token = token_elem.value;
	this.guild_select_elem = document.getElementById('scavenger_guild_select');

	this.initGateway();
}

Bot.prototype.prepare_api_rq = function(method, url, data)
{
	return new APIRequest(method, this.api_root + '/v' + this.api_http_version + url, data)
			.setHeader('Authorization', 'Bot ' + this.token);
}

Bot.prototype.prepare_cdn_rq = function(url, data)
{
	return new APIRequest('GET', this.res_root + url, data);
}

Bot.prototype.initGateway = function()
{
	let rq = new APIRequest('GET', this.api_root + '/gateway', null);
	let ans = rq.send();

	ans.then((req) => {
		try
		{
			let response = JSON.parse(req.responseText);
			if (typeof response.url === 'undefined')
				throw 'Missing url param';

			this.api_gateway = response.url;
		}
		catch (ex)
		{
			console.log('Unable to decode the gateway answer: ' + ex);
			return;
		}

		console.log('Received gateway: ' + this.api_gateway);

		this.initWS();
	}, (req) => {
		console.log('An error ' + req.status + ' (' + req.statusText + ') occurred while fetching the gateway.');
		window.setTimeout(() => this.initGateway(), 5000);
	});
	
}

Bot.prototype.send_op = function (op, data)
{
	let d = {};

	d.op = op;

	if (typeof data !== 'undefined')
		d.d = data;

	d = JSON.stringify(d);

	console.log('ws_send(' + d + ')');
	return this.ws.send(d);
};

Bot.prototype.send_heartbeat = function ()
{
	return this.send_op(this.OP_HEARTBEAT, this.last_seq);
};

Bot.prototype.send_ident = function(tok, intents, props, presence)
{
	if (intents === undefined)
		intents = 0;
	if (props === undefined)
		props = {
			"os": "",
			"browser": "",
			"device": "",
			"browser_user_agent": "",
			"browser_version": "",
			"os_version": "",
			"referrer": "",
			"referring_domain": "",
			"referrer_current": "",
			"referring_domain_current": ""
		};
	if (presence === undefined)
		presence = this.presence;

	return this.send_op(this.OP_IDENT, {
		token: tok,
		intents: intents,
		properties: props,
		presence: presence
	});
};

Bot.prototype.send_upd_status = function(presence)
{
	return this.send_op(this.OP_UPD_STATUS, presence);
};

Bot.prototype.send_req_guild_members = function(guild_id, query, limit, presences)
{
	if (query === undefined)
		query = '';
	if (limit === undefined)
		limit = 0;
	if (presences === undefined)
		presences = false;

	return this.send_op(this.OP_REQ_GUILD_MEMBERS, {
		guild_id: guild_id,
		query: query,
		limit: limit,
		presences: presences
	});
}

Bot.prototype.api_get_channel_messages_before = function(id_chan, id_msg_before)
{
	let params = '?limit=100';

	if (typeof id_msg_before !== 'undefined')
		params += '&before=' + id_msg_before;

	let rq = this.prepare_api_rq('GET', '/channels/' + id_chan + '/messages' + params);
	let ans = rq.send();

	return ans.then((req) => {
		console.log('Received messages for channel #' + id_chan);
		return JSON.parse(req.responseText);
	}, (req) => {
		let err_msg = 'Failed to get messages of channel #' + id_chan + ': ' + req.status + '(' + req.statusText + ')';
		console.log(err_msg);
		return Promise.reject(err_msg);
	});
};

Bot.prototype.cdn_get_user_avatar_data = function(id_user, user_avatar)
{
	if (user_avatar === null)
		return Promise.reject('User avatar is null');

	let file_ext = '.webp';
	let file_mime = 'image/webp';
	let filename = id_user + '/' + user_avatar + file_ext;

	let rq = this.prepare_cdn_rq('/avatars/' + filename);

	rq.setResponseType('arraybuffer');
	rq.overrideMimeType(file_mime);

	let ans = rq.send();

	return ans.then((req) => {
		console.log('Received avatar for user #' + id_user);
		return {
			filename: filename,
			data: req.response
		};
	}, (req) => {
		console.log('Failed to get avatar for user #' + id_user + ': ' + req.status + '(' + req.statusText + ')');
		return Promise.reject(req.statusText);
	});
};

Bot.prototype.cdn_get_attachment_data = function(id_channel, id_attachment, attachment_filename)
{
	let filename = id_channel + '/' + id_attachment + '/' + attachment_filename;
	let rq = this.prepare_cdn_rq('/attachments/' + filename);

	rq.setResponseType('arraybuffer');
	rq.overrideMimeType('application/x-binary');

	let ans = rq.send();

	return ans.then((req) => {
		console.log('Received attachment for ' + filename);
		return {
			filename: filename,
			data: req.response
		};
	}, (req) => {
		console.log('Failed to get attachment for ' + filename + ': ' + req.status + '(' + req.statusText + ')');
		return Promise.reject(req.statusText);
	});
};

Bot.prototype.cdn_get_emoji_data = function(id_emoji)
{
	let rq_cb = (file_ext, file_mime) => {
		let filename = id_emoji + file_ext;
		let rq = this.prepare_cdn_rq('/emojis/' + filename);

		rq.setResponseType('arraybuffer');
		rq.overrideMimeType(file_mime);

		return rq.send().then((req) => ({
			filename: filename,
			req: req
		}));
	};

	let ans = rq_cb('.gif', 'image/gif')
			.catch(() => rq_cb('.png', 'image/png'));

	return ans.then((res) => {
		console.log('Received emoji #' + id_emoji);
		return {
			filename: res.filename,
			data: res.req.response
		};
	}, (req) => {
		console.log('Failed to get emoji #' + id_emoji + ': ' + req.status + '(' + req.statusText + ')');
		return Promise.reject(req.statusText);
	});
};

function format_text(text)
{
	return text.replace(/(["&<>])/g, (whole, html_special_char) => {
		if (typeof html_special_char !== 'undefined')
		{
			const rpl = {
				'"': '&quot;',
				'&': '&amp;',
				'<': '&lt;',
				'>': '&gt;'
			};

			return rpl[html_special_char];
		}
	});
}

Bot.prototype.onGuildCreate = function(guild)
{
	if (guild.unavailable)
	{
		console.log('Skipped guild #' + guild.id + ': Unavailable.');
		return;
	}

	if (this.guilds[guild.id] !== undefined)
	{
		console.log('Skipped guild #' + guild.id + ': Already created.');
		return;
	}

	this.guilds[guild.id] = guild;

	console.log('Guild created: ' + guild.name + ' #' + guild.id);

	let button_elem = document.createElement('button');
	button_elem.innerHTML = format_text(guild.name);
	button_elem.style = "display:block;"

	this.guild_select_elem.appendChild(button_elem);
	button_elem.addEventListener('click', (ev) => {
		this.onGuildValidate(guild);
	});
}

Bot.prototype.onGuildValidate = function(guild)
{
	this.setStatusMsg("Scavenging...");

	guild.members = [];
	guild.presences = [];
	this.send_req_guild_members(guild.id, '', 0, true);
}

Bot.prototype.onGuildMembersChunk = function(chunk)
{
	let guild = this.guilds[chunk.guild_id];

	console.log('Received members chunk ' + (chunk.chunk_index + 1) + '/' + chunk.chunk_count + ' for guild #' + guild.id);

	guild.members = guild.members.concat(chunk.members);
	guild.presences = guild.presences.concat(chunk.presences);

	if (chunk.chunk_index == chunk.chunk_count - 1)
		this.onGuildGotAllMembers(guild);
}

Bot.prototype.onGuildGotAllMembers = function(guild)
{
	let guild_chans = guild.channels;
	let guild_users = {};
	let guild_msgs = {};
	let attachments = {};
	let avatars = {};
	let emojis = {};

	for (i in guild.members)
	{
		let member = guild.members[i];

		if (!guild.members.hasOwnProperty(i))
			continue;

		if (!guild_users[member.user.id])
			guild_users[member.user.id] = member.user;

		member.user = {
			id: member.user.id
		};
	}

	let msgs_rqs = [];
	let attachments_rqs = {};
	let emojis_rqs = {};

	for (let i in guild_chans)
	{
		let chan = guild_chans[i];

		if (!guild_chans.hasOwnProperty(i))
			continue;

		guild_msgs[chan.id] = [];

		console.log('Reading messages for channel #' + chan.id + ' "' + chan.name + '".');

		if (typeof chan.last_message_id === 'undefined')
		{
			console.log('Skipping empty channel #' + chan.id + ' "' + chan.name + '".');
			continue;
		}

		let fetch_channel_msgs_callback = (last_message_id) => {
			return this.api_get_channel_messages_before(chan.id, last_message_id)
					.then((msgs) => {
				if (msgs.length <= 0)
					return;

				for (i in msgs)
				{
					let msg = msgs[i];

					if (!msgs.hasOwnProperty(i))
						continue;

					if (!guild_users[msg.author.id])
						guild_users[msg.author.id] = msg.author;

					msg.author = {
						id: msg.author.id
					};

					if (msg.attachments !== null)
					{
						for (let attachment of msg.attachments)
						{
							if (attachment.url === null)
								continue;

							if (attachments_rqs[attachment.id] !== undefined)
								continue;

							let attach_result = attachment.url.match(/^https?:\/\/cdn\.discordapp\.com\/attachments\/([0-9]+)\/([0-9]+)\/([^\/]+)$/);
							if (attach_result === null && attachment.proxy_url !== null)
								attach_result = attachment.proxy_url.match(/^https?:\/\/media\.discordapp\.net\/attachments\/([0-9]+)\/([0-9]+)\/([^\/]+)$/);

							if (attach_result === null)
								continue;

							let rq = this.cdn_get_attachment_data(attach_result[1], attach_result[2], attach_result[3])
									.then((result) => {
								attachments[result.filename] = result.data;
							}, (reason) => {});

							attachments_rqs[attachment.id] = rq;
						}
					}

					if (msg.content !== null)
					{
						let emoji_results = msg.content.matchAll(/<a?:[A-Za-z0-9_]+:([0-9]+)>/g);
						for (let emoji_result of emoji_results)
						{
							let id_emoji = emoji_result[1];

							if (emojis_rqs[id_emoji] !== undefined)
								continue;

							let rq = this.cdn_get_emoji_data(id_emoji)
									.then((result) => {
								emojis[result.filename] = result.data;
							}, (reason) => {});

							emojis_rqs[id_emoji] = rq;
						}
					}
				}

				guild_msgs[chan.id] = guild_msgs[chan.id].concat(msgs);

				return fetch_channel_msgs_callback(msgs[msgs.length - 1].id);
			}, (reason) => {});
		}

		msgs_rqs[msgs_rqs.length] = fetch_channel_msgs_callback();
	}

	let msgs_rq = Promise.all(msgs_rqs);

	let attachments_rq = msgs_rq.then(() => {
		return Promise.all(Object.values(attachments_rqs));
	});

	let emojis_rq = msgs_rq.then(() => {
		return Promise.all(Object.values(emojis_rqs));
	});

	let avatars_rq = msgs_rq.then(() => {
		let avatars_rqs = [];

		for (let i in guild_users)
		{
			let user = guild_users[i];

			if (!guild_users.hasOwnProperty(i))
				continue;

			avatars_rqs[avatars_rqs.length] = this.cdn_get_user_avatar_data(user.id, user.avatar)
					.then((result) => {
				avatars[result.filename] = result.data;
			}, (reason) => {});
		}

		return Promise.all(avatars_rqs);
	});

	Promise.all([msgs_rq, attachments_rq, emojis_rq, avatars_rq]).then(() => {
		let info = {
			uid: '' + Date.now() + ((Math.random() * Number.MAX_SAFE_INTEGER) | 0),
			guild_id: guild.id
		};

		let tar = new TarFile()
				.appendFileAsString('info.json', JSON.stringify(info))
				.appendFileAsString('guild.json', JSON.stringify(guild))
				.appendFileAsString('users.json', JSON.stringify(guild_users));

		for (let i in guild_msgs)
		{
			let msgs = guild_msgs[i];

			if (!guild_msgs.hasOwnProperty(i))
				continue;

			tar.appendFileAsString('channel-' + i + '.json', JSON.stringify(msgs));
		}

		for (let i in attachments)
		{
			let attachment = attachments[i];

			if (!attachments.hasOwnProperty(i))
				continue;

			tar.appendFileAsArrayBuffer('attachments/' + i, attachment);
		}

		for (let i in avatars)
		{
			let avatar = avatars[i];

			if (!avatars.hasOwnProperty(i))
				continue;

			tar.appendFileAsArrayBuffer('avatars/' + i, avatar);
		}

		for (let i in emojis)
		{
			let emoji = emojis[i];

			if (!emojis.hasOwnProperty(i))
				continue;

			tar.appendFileAsArrayBuffer('emojis/' + i, emoji);
		}

		let blob = tar.toBlob();
		let blob_url = URL.createObjectURL(blob);

		let a_elem = document.createElement("a");
		a_elem.hidden = true;
		a_elem.href = blob_url;
		a_elem.rel = 'noreferrer noopener';
		a_elem.target = '_blank';
		a_elem.download = guild.name + '.tar';
		document.body.appendChild(a_elem);
		a_elem.click();

		URL.revokeObjectURL(blob_url);

		this.setStatusMsg("Idle.");
	});
}

Bot.prototype.setStatusMsg = function(msg)
{
	this.presence = {
		status: 'online',
		afk: false,
		since: null,
		game: {
			name: msg,
			type: 0,
			timestamps: {
				start: Date.now()
			},
			application_id: '485212588938887169',
			assets: {
				large_image: '561893023282102277',
				large_text: ':calimvasion:',
				small_image: '561904324209541121',
				small_text: ':cailm:'
			}
		}
	};

	if (this.state === this.STATE_CONNECTED)
		this.send_upd_status(this.presence);
}

Bot.prototype.initWS = function()
{
	if (this.state !== this.STATE_INIT_WS)
	{
		console.log('Internal error: expected state ' + this.STATE_INIT_WS + ', got ' + this.state + '.');
		return;
	}

	this.ws = new WebSocket(this.api_gateway + '?v=' + this.api_gateway_version + '&encoding=json');

	this.ws.onopen = (ev) => {
		console.log('ws_open()');
	};

	this.ws.onmessage = (ev) => {
		let data;
		let d;

		try
		{
			data = JSON.parse(ev.data);
		}
		catch (ex)
		{
			console.log('Invalid msg: ' + ex + ': ' + ev.data + ')');
			return;
		}

		d = data.d;

		console.log('ws_msg(' + ev.data + ')');

		switch (data.op)
		{
			case this.OP_DISPATCH:
				this.last_seq = data.s;

				console.log('DISPATCH.' + data.t);

				switch (data.t)
				{
					case 'READY':
						this.state = this.STATE_CONNECTED;

						for (let guild of d.guilds)
							this.onGuildCreate(guild);
						break;

					case 'GUILD_CREATE':
						this.onGuildCreate(d);
						break;

					case 'GUILD_MEMBERS_CHUNK':
						this.onGuildMembersChunk(d);
						break;
				}
				break;

			case this.OP_HEARTBEAT:
				console.log('HEARTBEAT');
				this.send_heartbeat();
				break;

			case this.OP_HEARTBEAT_ACK:
				console.log('HEARTBEAT_ACK');
				break;

			case this.OP_INVAL_SESSION:
				console.log('INVAL_SESSION');
				break;

			case this.OP_HELLO:
				console.log('HELLO');
				this.heartbeat_interval = d.heartbeat_interval;

				this.heartbeat_interval_id = window.setInterval(() => {
					this.send_heartbeat();
				}, this.heartbeat_interval);

				this.setStatusMsg("Idle.");
				this.send_ident(this.token, this.GW_INTENT_GUILD
						| this.GW_INTENT_GUILD_MEMBERS
						| this.GW_INTENT_GUILD_PRESENCES);
				break;
		}
	};

	this.ws.onerror = (ev) => {
		console.log('ws_err()');
	};

	this.ws.onclose = (ev) => {
		console.log('ws_close(' + ev.code + ')');

		if (ev.code === this.GW_ERR_DISALLOWED_INTENT)
			console.log("Failed to identify: Disallowed intents.\n**** Please enable the following privileged intents for the application in the developper portal: Members, Presences. ****");

		window.clearInterval(this.heartbeat_interval_id);
		return;
	};
}

