function TarFile()
{
	this.parts = [];
	this.enc = new TextEncoder();
}

TarFile.prototype.appendFile = function(filename, data)
{
	let header = new Uint8Array(512);
	let fname = this.enc.encode(filename);
	let ftype = '0';

	if (fname.length > 100)
		this.appendFile('././@LongLink', fname);

	if (filename === '././@LongLink')
		ftype = 'L';

	if (typeof data === 'string')
		data = this.enc.encode(data);

	let num_encode = (num, pad) => this.enc.encode(num.toString(8).padStart(pad, '0'));

	header.set(fname.subarray(0, 100), 0);
	header.set(num_encode(0644, 7), 100);
	header.set(num_encode(0, 7), 108);
	header.set(num_encode(0, 7), 116);
	header.set(num_encode(data.length, 11), 124);
	header.set(num_encode(0, 11), 136);
	header.set(this.enc.encode('        '), 148);
	header.set(this.enc.encode(ftype), 156);
	header.set(this.enc.encode('ustar'), 257);
	header.set(num_encode(0, 2), 263);
	header.set(this.enc.encode('root'), 265);
	header.set(this.enc.encode('root'), 297);
	header.set(num_encode(0, 8), 329);
	header.set(num_encode(0, 8), 337);

	let checksum = header.reduce((old, cur) => old + cur);
	header.set(num_encode(checksum, 6), 148);
	header.set([0], 154);

	let d = new Uint8Array(Math.ceil(data.length / 512) * 512);
	d.set(data, 0);

	this.parts.push(header);
	this.parts.push(d);

	return this;
}

TarFile.prototype.appendFileAsArrayBuffer = function(filename, buffer)
{
	return this.appendFile(filename, new Uint8Array(buffer));
}

TarFile.prototype.appendFileAsString = function(filename, str)
{
	let data = this.enc.encode(str);
	return this.appendFile(filename, data);
}

TarFile.prototype.toBlob = function()
{
	let parts = this.parts.slice();
	parts.push(new Uint8Array(2 * 512));
	return new Blob(parts, { type: 'application/x-tar' });
}
